import { Component, OnInit } from '@angular/core';
import { Post } from '../interface/post';
import { PostsService } from '../service/posts.service';
import { Comment } from '../interface/comment';
import { AuthService } from '../service/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.css']
})
export class BlogPostComponent implements OnInit {

  panelOpenState = false;
  $Posts:Post[];
  $Comments:Comment[];
  title:string;
  body:string;
  id:number;
  userId:string;

  constructor(private postsService:PostsService, private authService:AuthService, private route:ActivatedRoute) { }

  ngOnInit() {
    return this.postsService.getPosts().subscribe(data => this.$Posts = data),
           this.postsService.getComments().subscribe(data => this.$Comments = data)
  }

  SavePost(idOfPost:number){
    this.id = this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user => {
      this.userId = user.uid;
      if(this.id)  { 
      let arrayObj = this.$Posts;
      for (let i = 0; i < arrayObj.length; i++) {
        if (this.$Posts[i].id == idOfPost){
          this.postsService.addPost(this.userId,this.$Posts[i].id,this.$Posts[i].title,this.$Posts[i].body);
        }
      } 
    alert("Saved for later viewing!");
}
})
}
}
