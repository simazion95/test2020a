import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  email:string;
  password:string;

  onSubmitSignUp(){
    this.authService.SignUp(this.email,this.password);
    this.router.navigate(['/welcome']);
  }

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

}
