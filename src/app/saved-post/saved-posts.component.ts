import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostsService } from '../service/posts.service';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-saved-posts',
  templateUrl: './saved-posts.component.html',
  styleUrls: ['./saved-posts.component.css']
})
export class SavedPostsComponent implements OnInit {

  panelOpenState = false;
  $FBPosts:Observable<any[]>
  userId:string;
  likes = 0;

  deletePost(id:string){
    this.postsService.deletePost(this.userId,id)
  }

  addLikes(){
    this.likes++
  }

  constructor(private postsService:PostsService, private authService:AuthService) { }

  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.$FBPosts = this.postsService.getAllPosts(this.userId); 
      }
    )
  }

}
