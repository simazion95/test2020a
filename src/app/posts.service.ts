import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { Post } from '../interface/post';
import { Comment } from '../interface/comment';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private urlPosts = "https://jsonplaceholder.typicode.com/posts/";
  private urlComments = "https://jsonplaceholder.typicode.com/comments";

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  postsCollection:AngularFirestoreCollection

  getPosts(){
        return this.http.get<Post[]>(this.urlPosts);
    
      }

  getComments(){
    return this.http.get<Comment[]>(this.urlComments);

  }

  addPost(id:number,title:string,body:string){
    const post = {id:id,title:title,body:body}
    this.db.collection('posts').add(post);

  }

  deletePost(userId:string, id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }

 
  getAllPosts(userId:string){
    this.postsCollection = this.db.collection(`users/${userId}/posts`);
    return this.postsCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
                const data = document.payload.doc.data();
                data.id = document.payload.doc.id;
                return data;
      }))
    );  
    
  }

  constructor(private http:HttpClient, private db:AngularFirestore, private authService:AuthService) { }
}
