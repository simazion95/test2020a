import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { User } from '../interface/user';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;

  SignUp(email:string, password:string){
        this.afAuth
            .auth
            .createUserWithEmailAndPassword(email,password)
            .then(res =>
              {
              console.log('Succesful sign up',res);
              }
            );
  }

  getUser(){
    return this.user
  }

  Logout(){
    this.afAuth.auth.signOut().then(res =>console.log('Succesful logout',res))
    this.router.navigate(['/welcome']); 
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
          
          res => 
          {   console.log('Succesful Login',res);
              this.router.navigate(['/welcome']); 
          }
        )
  }


  constructor(private router:Router, public afAuth:AngularFireAuth) {
    this.user = this.afAuth.authState;
   }
}
