import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  userId:string;
  userEmail:string;

  constructor(public authService:AuthService) { }

  ngOnInit() {

    this.authService.user.subscribe(
              user => {
                this.userId = user.uid;
                this.userEmail = user.email;
               }
            )
  }

}
