// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyC4T388TTEt6A2NsnJ-6HR9SBfpyKeE-k8",
    authDomain: "test20-1dc2b.firebaseapp.com",
    databaseURL: "https://test20-1dc2b.firebaseio.com",
    projectId: "test20-1dc2b",
    storageBucket: "test20-1dc2b.appspot.com",
    messagingSenderId: "617401288951",
    appId: "1:617401288951:web:a6fabde5c933418d556bf2",
    measurementId: "G-0RFSV265WL"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
