export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyC4T388TTEt6A2NsnJ-6HR9SBfpyKeE-k8",
    authDomain: "test20-1dc2b.firebaseapp.com",
    databaseURL: "https://test20-1dc2b.firebaseio.com",
    projectId: "test20-1dc2b",
    storageBucket: "test20-1dc2b.appspot.com",
    messagingSenderId: "617401288951",
    appId: "1:617401288951:web:a6fabde5c933418d556bf2",
    measurementId: "G-0RFSV265WL"
  }
};